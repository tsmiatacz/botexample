package pl.tsmiatacz.botexample.bots;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import pl.tsmiatacz.botexample.service.TextService;
import pl.tsmiatacz.botplatform.behavior.Behavior;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.behavior.condition.MessageCondition;
import pl.tsmiatacz.botplatform.behavior.reaction.BotReaction;
import pl.tsmiatacz.botplatform.behavior.reaction.MessageReaction;
import pl.tsmiatacz.botplatform.bots.AbstractBot;
import pl.tsmiatacz.botplatform.service.ConfigurationService;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This is a bot that utilizes a hibernate database connection and responds to messages.
 * Needs privacy mode disabled and existing database (look at initialize.sql).
 */
public class DatabaseBot extends AbstractBot {

    private TextService textService;

    @Inject
    public DatabaseBot(final ConfigurationService configurationService, final TextService textService) {
        super(configurationService);
        this.textService = textService;
    }

    @Override
    public void initialize() {
        super.initialize();
        addBehaviors(createRepeatBehavior());
        createMessageRepliesBehaviors()
                .forEach(this::addBehaviors);
    }

    /**
     * When you address bot with "repeat" it will repeat text in quotes, eg.:
     * `@{database_bot_username} repeat "foo"` will reply with "foo".
     * We're implementing a custom RepeatReaction here.
     */
    private Behavior createRepeatBehavior() {
        return Behavior.of(
                MessageCondition.contains("repeat"),
                new RepeatReaction());
    }

    /**
     * Adds simple message response behavior for every text in database, eg.:
     * `@{database_bot_username} hello` will reply with "Hello world!"
     */
    private List<Behavior> createMessageRepliesBehaviors() {
        return textService.texts(getBotType()).stream()
                .map(text -> Behavior.of(
                        MessageCondition.endsWith(text.getKey()),
                        MessageReaction.reply(text.getValue())))
                .collect(Collectors.toList());
    }

    /**
     * Implementing a custom reaction.
     */
    private static class RepeatReaction extends BotReaction<Message> {

        @Override
        protected BotApiMethod<Message> createReaction(final BehaviorContext context) {
            var message = "Nothing to repeat :(";
            if (context.getArguments().size() > 1) {
                message = context.argument(0);
            }

            var sendMessage = new SendMessage(context.getChatId(), message);
            sendMessage.setChatId(context.getChatId());
            return sendMessage;
        }
    }
}
