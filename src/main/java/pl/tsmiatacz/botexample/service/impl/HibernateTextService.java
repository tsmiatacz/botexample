package pl.tsmiatacz.botexample.service.impl;

import com.google.common.collect.ImmutableList;
import org.hibernate.query.Query;
import pl.tsmiatacz.botexample.model.Text;
import pl.tsmiatacz.botexample.service.TextService;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class HibernateTextService extends AbstractHibernateService implements TextService {

    @Override
    public List<Text> texts(final String bot) {
        return executeInSession(session -> {
            final Query<Text> query = session.createQuery(
                    "SELECT t FROM Text t WHERE t.bot = :bot",
                    Text.class);
            query.setParameter("bot", bot);

            return ImmutableList.copyOf(query.getResultList());
        });
    }
}
