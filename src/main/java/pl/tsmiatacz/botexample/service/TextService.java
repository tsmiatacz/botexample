package pl.tsmiatacz.botexample.service;

import pl.tsmiatacz.botexample.model.Text;

import java.util.List;

public interface TextService {

    /**
     * Get a list of available texts from database.
     * @param bot not null
     * @return not null
     */
    List<Text> texts(String bot);
}
