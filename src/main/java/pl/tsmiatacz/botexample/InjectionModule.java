package pl.tsmiatacz.botexample;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import pl.tsmiatacz.botexample.service.LennyfaceService;
import pl.tsmiatacz.botexample.service.TextService;
import pl.tsmiatacz.botexample.service.impl.DefaultLennyfaceService;
import pl.tsmiatacz.botexample.service.impl.HibernateTextService;

/**
 * This is just a regular Guice DI module
 */
public class InjectionModule extends AbstractModule {
    @Override
    protected void configure() {
        super.configure();

        bind(TextService.class).to(HibernateTextService.class).in(Singleton.class);
        bind(LennyfaceService.class).to(DefaultLennyfaceService.class).in(Singleton.class);
    }
}
