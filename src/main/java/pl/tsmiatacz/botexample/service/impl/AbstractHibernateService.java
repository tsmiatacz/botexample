package pl.tsmiatacz.botexample.service.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.function.Consumer;
import java.util.function.Function;

public abstract class AbstractHibernateService {

    private static StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    private static SessionFactory sessionFactory = createSessionFactory();

    protected <R> R executeInSession(final Function<Session, R> function) {
        try (final Session session = openSession()) {
            return function.apply(session);
        }
    }

    protected void executeInTransaction(final Consumer<Session> consumer) {
        try (final Session session = openSession()) {
            final Transaction transaction = session.beginTransaction();
            consumer.accept(session);
            transaction.commit();
        }
    }

    private Session openSession() {
        return sessionFactory.openSession();
    }

    private static SessionFactory createSessionFactory() {
        final MetadataSources sources = new MetadataSources(registry);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}
