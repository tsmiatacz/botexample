package pl.tsmiatacz.botexample.bots;

import pl.tsmiatacz.botplatform.behavior.Behavior;
import pl.tsmiatacz.botplatform.behavior.condition.AdminCondition;
import pl.tsmiatacz.botplatform.behavior.condition.CommandCondition;
import pl.tsmiatacz.botplatform.behavior.reaction.LeaveReaction;
import pl.tsmiatacz.botplatform.behavior.reaction.MessageReaction;
import pl.tsmiatacz.botplatform.bots.AbstractBot;
import pl.tsmiatacz.botplatform.service.ConfigurationService;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * This is a simple bot responding to some commands.
 * Requires no additional settings.
 */
@Singleton
public class CommandBot extends AbstractBot {

    @Inject
    public CommandBot(final ConfigurationService configurationService) {
        super(configurationService);
    }

    /**
     * Register custom behaviors in this method.
     */
    @Override
    public void initialize() {
        super.initialize();
        addBehaviors(
                createPingBehavior(),
                createLeaveBehavior());
    }

    /**
     * Create a simple command behavior:
     * When a bot receives command "/ping" it will respond with "pong".
     */
    private Behavior createPingBehavior() {
        return Behavior.of(
                CommandCondition.of("/ping"),
                MessageReaction.simple("pong"));
    }

    /**
     * Create a more complex behavior:
     * When bot receives command "/leave" from administrator, it will respond with "leaving" and leave group.
     */
    private Behavior createLeaveBehavior() {
        return Behavior.of(
                CommandCondition.of("/leave")
                    .and(AdminCondition.of()),
                MessageReaction.simple("leaving")
                    .andThen(LeaveReaction.of()));
    }

}
