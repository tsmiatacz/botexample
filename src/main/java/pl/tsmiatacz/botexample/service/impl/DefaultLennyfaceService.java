package pl.tsmiatacz.botexample.service.impl;

import pl.tsmiatacz.botexample.service.LennyfaceService;
import pl.tsmiatacz.botplatform.data.TextData;

import javax.inject.Singleton;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * As an exercise you can refactor this service to use database.
 * Inline requests are cached by Telegram so it shouldn't kill your db.
 */
@Singleton
public class DefaultLennyfaceService implements LennyfaceService {

    private static final Map<String, TextData> LENNYFACES = Map.of(
            "lenny", TextData.of(1L, "( ͡° ͜ʖ ͡°)"),
            "wink", TextData.of(2L, "( ͡~ ͜ʖ ͡°)"),
            "shrug", TextData.of(3L, "¯\\_(ツ)_/¯"),
            "legion", TextData.of(4L, "( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)"),
            "frustrated", TextData.of(5L, "(╯°□°）╯︵ ┻━┻"),
            "smartass1", TextData.of(6L, "(⌐ ͡■ ͜ʖ ͡■)"),
            "smartass2", TextData.of(7L, "( ͡° ͜ʖ ͡°)ﾉ⌐■-■"));

    @Override
    public List<TextData> get(final String query) {
        return LENNYFACES.entrySet().stream()
                .filter(entry -> entry.getKey().startsWith(query))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }
}
