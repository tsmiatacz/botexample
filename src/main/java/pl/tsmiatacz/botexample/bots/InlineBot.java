package pl.tsmiatacz.botexample.bots;

import com.google.inject.Inject;
import pl.tsmiatacz.botexample.service.LennyfaceService;
import pl.tsmiatacz.botplatform.behavior.Behavior;
import pl.tsmiatacz.botplatform.behavior.condition.InlineCondition;
import pl.tsmiatacz.botplatform.behavior.reaction.InlineMessageReaction;
import pl.tsmiatacz.botplatform.bots.AbstractBot;
import pl.tsmiatacz.botplatform.service.ConfigurationService;

import javax.inject.Singleton;

/**
 * This bot will provide simple inline response.
 * Needs inline mode enabled in settings.
 */
@Singleton
public class InlineBot extends AbstractBot {

    private LennyfaceService lennyfaceService;

    @Inject
    public InlineBot(final ConfigurationService configurationService, final LennyfaceService lennyfaceService) {
        super(configurationService);
        this.lennyfaceService = lennyfaceService;
    }

    /**
     * Add custom behavior here.
     */
    @Override
    public void initialize() {
        addBehaviors(
                createInlineLennyfaceBehavior());
    }

    /**
     * When you type bot name in text input (but not send) followed by query it will display a list of available lennyfaces, eg.:
     * `@{inline_bot_username} ` - will display all lennyfaces
     * `@{inline_bot_username} shrug` will display ¯\_(ツ)_/¯
     * `@{inline_bot_username} smartass` will display "(⌐ ͡■ ͜ʖ ͡■) and ( ͡° ͜ʖ ͡°)ﾉ⌐■-■
     */
    private Behavior createInlineLennyfaceBehavior() {
        return Behavior.of(
                InlineCondition.of(),
                InlineMessageReaction.of(lennyfaceService::get));
    }
}
