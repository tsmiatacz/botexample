CREATE TABLE texts
(
    id    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    bot   TEXT    NOT NULL,
    key   TEXT,
    value TEXT    NOT NULL
);

INSERT INTO texts (bot, key, value)
VALUES ('DatabaseBot', 'hello', 'Hello world!');
INSERT INTO texts (bot, key, value)
VALUES ('DatabseBot', 'sing', 'Na na na na na na na');