# Botplatform examples  
  
## Prerequisites
1. Create 3 telegram bots (you can name them whatever you want):    
    - command bot (no special settings needed)    
    - inline bot (inline mode enabled)  
    - database bot (privacy mode disabled)
  
2. Create a new group and add your bots there. All bot exceptions will be logged in this group.  
  
3. Install sqlite3.
   
## Running  
1. Replace following placeholders in dev.properties:  
    - {log_group} - identifier of log group (negative number)  
    - {admin_username} - your username 
    - {command_bot_username} - command bot username  
    - {command_bot_key} - command bot key  
    - {inline_bot_username} - inline bot username  
    - {inline_bot_key} - inline bot key  
    - {database_bot_username} - database bot username  
    - {database_bot_key} - database bot key  
  
2. Create a database by going to /db and executing following commands:  
    `sqlite3 bots.db`    
    `.read initialize.sql`    
  
3. You should be able to execute Runner class.

## Development
Just take a look at how `CommandBot`, `InlineBot` and `DatabaseBot` are implemented.