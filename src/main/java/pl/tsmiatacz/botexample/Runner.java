package pl.tsmiatacz.botexample;


import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import pl.tsmiatacz.botplatform.Application;

public class Runner {

    public static void main(String[] args) throws TelegramApiException {
        Application.run("dev", new InjectionModule());
    }
}