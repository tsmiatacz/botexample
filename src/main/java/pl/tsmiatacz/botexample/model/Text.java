package pl.tsmiatacz.botexample.model;

import jakarta.persistence.*;

@Entity
@Table(name = "texts")
public class Text {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    private Long id;

    @Column(name = "bot")
    private String bot;

    @Column(name = "key")
    private String key;

    @Column(name = "value")
    private String value;

    public long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getBot() {
        return bot;
    }

    public void setBot(final String bot) {
        this.bot = bot;
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
