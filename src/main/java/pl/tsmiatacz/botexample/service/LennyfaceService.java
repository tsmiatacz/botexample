package pl.tsmiatacz.botexample.service;

import pl.tsmiatacz.botplatform.data.TextData;

import java.util.List;

public interface LennyfaceService {

    /**
     * Get a list of lennyfaces for given query. Will return all faces when query is empty.
     * @param query not null
     * @return not null
     */
    List<TextData> get(String query);
}
